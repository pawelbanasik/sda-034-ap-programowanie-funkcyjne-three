package com.pawelbanasik;

public interface NumberGenerator {
	int generate();
}
